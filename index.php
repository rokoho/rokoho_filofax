<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.Filofax
 *
 * @copyright   Copyright (C) 2014 Rokoho Webdesign.
 */

defined('_JEXEC') or die;

$params = JFactory::getApplication()->getTemplate(true)->params;
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;
unset($this->_scripts['/budget/media/system/js/mootools-core.js']); 


$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');

if($this->params->get('alt_sitename') !=''):$sitename = $this->params->get('alt_sitename');else:$sitename = $app->getCfg('sitename');endif;

// Add JavaScript Frameworks
$doc->addScript('templates/' .$this->template. '/js/bootstrap.min.js');

// Add Stylesheets
$doc->addStyleSheet('templates/'.$this->template.'/css/bootstrap-min.css');
$doc->addStyleSheet('templates/'.$this->template.'/css/template.css');
$doc->addStyleSheet('libraries/font-awesome/css/font-awesome.css');


if($this->params->get('tlogo')):
	$logo = '<img src="' . $this->params->get('tlogo') . '" class="img-circle" alt="' . $sitename . '" />';
endif;
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
  <script>var style_cookie_name = '<?php echo $this->template; ?>' ;</script>
<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge" /> not valid xhtml -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
<jdoc:include type="head" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800' rel='stylesheet' type='text/css'>
<!--[if IE]>
  <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet" type="text/css">
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400" rel="stylesheet" type="text/css">
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet" type="text/css">
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:800" rel="stylesheet" type="text/css">
<![endif]-->
<link rel="shortcut icon" href="templates/<?php echo $this->template ?>/ico/favicon.png" />
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<script type="text/javascript" src="templates/<?php echo $this->template ?>/js/jquery.smooth-scroll.min.js"></script>
<script type="text/javascript" src="templates/<?php echo $this->template ?>/js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="templates/<?php echo $this->template ?>/js/waypoints.min.js"></script>
</head>
<body data-spy="scroll" data-offset="100" data-target=".navigator" onload="switch_style( style ); return false;" ><i id="ltop"></i>
<div id="mobile-menu" class="" ></div>
<div class="content sheet">
  <div class="container">
    <div class="inner-container">
      <?php if ($this->countModules( 'user1' )) : ?>
      <div class="alert">
        <jdoc:include type="message" />
      </div>
    <?php endif; ?>
      <div class="row">
        <div class="col-md-3" id="leftcol">
          <div  class="leftcol-inner">
            <a class="brand logo smoothscroll" href="#ltop"><?php // echo $logo; ?></a>

            <div id="leftnav" class="navigator">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
           <span class="sr-only">Toggle navigation</span> 
           <i class="fa fa-bars"></i> 
         </button>
              <div class="navbar-collapse collapse">
                <jdoc:include type="modules" name="collapse" />
              </div>
            </div>
          </div>
        </div>
          <div class="col-md-9 col-content"  id="rightcol">
            <div class="rightcol-inner">
              <header>
                <div id="mobile-logo"></div>
                <h1>
                  <a class="brand smoothscroll" href="#ltop"><?php echo $sitename; ?></a>
                </h1>
              </header>
              <jdoc:include type="component" />
              <hr class="featurette-divider">
              <jdoc:include type="modules" name="postcontent" style="html5" />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-9 col-md-offset-3">
          <div id="append-target"><div class="wrap waypoint"></div></div>
        </div>
      </div>  
      <jdoc:include type="modules" name="footer" style="html5" />
    </div>
  </footer>
  <script>
  	( function($) {
      $(document).ready(function() {

        $("#leftcol a.brand.logo").clone().appendTo("#mobile-logo");

  /*
        $('#leftcol ul.nav') .css({'min-width': $('#leftcol').width()+'px'});
        $(window).resize(function(){
          $('#leftcol ul.nav') .css({'min-width': $('#leftcol').width()+'px'});
        });

        $('.item-wrap') .css({'padding-top': $('#mobile-menu ul').height()+'px'});

        $('header') .css({'padding-top': $('#mobile-menu ul').height()+'px'});
        
        $(window).resize(function(){
          $('.item-wrap') .css({'padding-top': $('#mobile-menu').height()+'px'});
          $('header') .css({'padding-top': $('#mobile-menu ul').height()+'px'});
        });

  */
        $('#append-target') .css({'min-height': $('.op_panel:last-child .item-wrap .item').height()+'px'});
        $(window).resize(function(){
          $('#append-target') .css({'min-height': $('.op_panel:last-child .item-wrap .item').height ()+'px'});
        });




        $('.op_panel:last-child .item-wrap').appendTo( $('#append-target .wrap') );




        $('.op_panel:first-child').waypoint(function(direction) {
          if (direction == 'down') {
             $("span.fa-stack").animate({margin:"0 15px"}, 'slow');
          }
          else {
             $("span.fa-stack").animate({margin:"0 5px"});
          }
        }, {offset: -230});

        $('#panel-2').waypoint(function(direction) {
         if (direction == 'down') {
             $('#albumCarousel div.col-md-4 img').animate({padding:"5px"});
          }
          else {
             $('#albumCarousel div.col-md-4 img ').animate({padding:"0px"});
           }
        }, {offset: 400});





        $('a.smoothscroll').smoothScroll({
  		  
      		 afterScroll: function() {},
       		 easing: 'swing',
       		 speed: 900,
      		 offset:-55
    
    		});



        //$('.content').on('click', function(){ $(".navbar-collapse").collapse('hide'); });


      });
   } ) ( jQuery );


   // Mootools-more - Bootstrap carousel fix
   
   if (typeof jQuery != 'undefined' && typeof MooTools != 'undefined' ) {
      Element.implement({
          slide: function(how, mode){
              return this;
          }
      });
  	
  	

  }

   
    </script>
</body>
</html>